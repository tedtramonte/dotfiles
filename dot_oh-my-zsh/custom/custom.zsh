ZSH_THEME="powerlevel10k/powerlevel10k"

export PATH=$HOME/.local/bin:$HOME/bin:/usr/local/bin:$PATH

alias lsl="ls -l"
alias lsa="ls -a"
alias lsal="ls -al"

# Enable completions
autoload -U compinit && compinit
