# Dotfiles

My customized configuration files. Configuration is managed by the [Chezmoi](https://www.chezmoi.io) CLI tool.

## Requirements

- [tedtramonte/ansible](https://gitlab.com/tedtramonte/ansible) for installation and more information

## Usage

- `chezmoi init --apply https://gitlab.com/tedtramonte/dotfiles.git` will initialize everything
  - remove the `--apply` flag to prevent changes from being made until `chezmoi apply` is run
- `chezmoi update` will pull and apply the latest repo changes
