#!/bin/sh
[ "${TERM:-none}" = "linux" ] && \
    printf '%b' '\e]P0073642
                 \e]P1dc322f
                 \e]P2859900
                 \e]P3b58900
                 \e]P4268bd2
                 \e]P5d33682
                 \e]P62aa198
                 \e]P7eee8d5
                 \e]P86c7c80
                 \e]P9dc322f
                 \e]PA859900
                 \e]PBb58900
                 \e]PC268bd2
                 \e]PDd33682
                 \e]PE2aa198
                 \e]PFeee8d5
                 \ec'
